import {Routes, Route} from "react-router-dom";
import Navigation from "./component/Navigation";
import Dashboard from "./component/Dashboard";
import Todolist from "./component/Todolist";
import './App.css'
import {useState} from "react";

function App() {
    const [todolist, setTodolist] = useState(getTodolistLocalStorage());

    return (
        <div id='main-content'>
            <Navigation/>
            <Routes>
                <Route path='/' element={<Dashboard list={todolist} callback={setTodolist}/>}/>
                <Route path='/todolist' element={<Todolist list={todolist} callback={setTodolist}/>}/>
            </Routes>
        </div>
    )
}

function getTodolistLocalStorage() {
    let storageTodolist = localStorage.getItem('todolist')
    if (storageTodolist) {
        return JSON.parse(storageTodolist)
    } else {
        return []
    }
}

export default App;