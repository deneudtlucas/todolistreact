import AddListItem from "./model/AddListItem";

function Todolist(props) {
    return (
        <div className='todolist'>
            <h1>Ajouter un item</h1>
            <AddListItem callback={props.callback} list={props.list}/>
        </div>
    )
}

export default Todolist;