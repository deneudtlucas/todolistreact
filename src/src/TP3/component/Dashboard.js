import List from "./model/List";

function Dashboard(props) {
    return (
        <div className='dashboard'>
            <h1>Dashboard</h1>
            <List list={props.list} callback={props.callback}/>
        </div>
    )
}

export default Dashboard;