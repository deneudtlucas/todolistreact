import {NavLink} from "react-router-dom";

function Navigation() {
    const links = [
        {to: '/', key: 'dashboard', label: 'Dashboard'},
        {to: '/todolist', key: 'todolist', label: 'Todolist'},
    ]

    return (
        <nav className='todolist-nav'>
            {
                links.map(({to, key, label}) => {
                    return <NavLink to={to} key={key} className={({isActive}) => {
                        return isActive ? 'active' : null
                    }}>{label}</NavLink>
                })
            }
        </nav>
    )
}

export default Navigation;