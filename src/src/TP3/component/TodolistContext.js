import {createContext} from "react";

export const TodolistContext = createContext({
    list: [],
    callback: () => {
    },
})