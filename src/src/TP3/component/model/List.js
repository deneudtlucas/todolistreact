import ListItem from "./ListItem";
import {useEffect} from "react";

function List(props) {
    let i = -1;
    return (
        <ul>
            {
                props.list.map(({image, title, description, deadline, status}) => {
                    i++
                    return <ListItem key={i} image={image} title={title} description={description} deadline={deadline}
                                     status={status} id={i} list={props.list} callback={props.callback}/>
                })
            }
        </ul>

    )
}

export default List;