import {useState} from "react";
import saveTodolistLocalStorage from "../../helper/TodolistService";

function ListItem(props) {
    const [image, setImage] = useState(null);
    const [title, setTitle] = useState(null);
    const [description, setDescription] = useState(null);
    const [deadline, setDeadline] = useState(null);

    return (
        <div className='add-item'>
            <div>
                <span>Image: </span>
                <input type='url' className='list-image' onChange={(event) => {
                    setImage(event.target.value)
                }}/>
            </div>
            <div>
                <span>Titre: </span>
                <input type='text' className='list-title' onChange={(event) => {
                    setTitle(event.target.value)
                }}/>
            </div>
            <div>
                <span>Description: </span>
                <input type='text' className='list-description' onChange={(event) => {
                    setDescription(event.target.value)
                }}/>
            </div>
            <div>
                <span>Deadline: </span>
                <input type='date' className='list-deadline' onChange={(event) => {
                    setDeadline(event.target.value)
                }}/>
            </div>
            <button onClick={() => addItem(image, title, description, deadline, props)}>Ajouter un truc à
                faire
            </button>
        </div>
    )
}

function addItem(image, title, description, deadline, props) {
    const status = false;
    let item = {image, title, description, deadline, status};
    props.list.push(item);
    props.callback(props.list);
    saveTodolistLocalStorage(props.list)
}

export default ListItem;