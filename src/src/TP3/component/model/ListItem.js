import saveTodolistLocalStorage from "../../helper/TodolistService";

function ListItem(props) {

    if (!props.status) {
        return <li className='list-item'>
            <img alt={props.title} className='list-image' src={props.image}/>
            <span className='list-title'>{props.title}</span>
            <span className='list-description'>{props.description}</span>
            <span className='list-deadline'>{props.deadline}</span>
            <button onClick={() => removeItem(props)}>Supprimer</button>
            <button onClick={() => editItem(props)}>Editer</button>
            <button onClick={() => checkItem(props)}>Check</button>
        </li>
    } else {
        return null
    }
}

function checkItem(props) {
    props.list[props.id].status = true;
    props.callback(props.list);
    saveTodolistLocalStorage(props.list)
}

function editItem(props) {
}

function removeItem(props) {
    props.list.splice(props.id, props.id + 1);
    props.callback(props.list);
    saveTodolistLocalStorage(props.list)
}

export default ListItem;