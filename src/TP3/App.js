import {Route, Routes} from "react-router-dom";
import Navigation from "./component/Navigation";
import Dashboard from "./component/Dashboard";
import Todolist from "./component/Todolist";
import {useState} from "react";
import AddTodolist from "./component/AddTodolist";
import EditListItem from "./component/model/EditListItem";
import {Box, Grid} from "@mui/material";

function App() {
    const [todolist, setTodolist] = useState(getTodolistLocalStorage());

    return (
        <Box id='main-content'>
            <Navigation/>
            <Grid sx={{pl: '5%', pr: '5%'}}>
                <Routes>
                    <Route path='/' element={<Dashboard list={todolist} callback={setTodolist}/>}/>
                    <Route path='/inProgress'
                           element={<Todolist list={todolist} callback={setTodolist} finished={false}/>}/>
                    <Route path='/finished'
                           element={<Todolist list={todolist} callback={setTodolist} finished={true}/>}/>
                    <Route path='/add' element={<AddTodolist list={todolist} callback={setTodolist}/>}/>
                    <Route path='/edit' element={<EditListItem list={todolist} callback={setTodolist}/>}/>
                </Routes>
            </Grid>
        </Box>
    )
}

function getTodolistLocalStorage() {
    let storageTodolist = localStorage.getItem('todolist')
    if (storageTodolist) {
        return JSON.parse(storageTodolist)
    } else {
        return []
    }
}

export default App;