import AddListItem from "./model/AddListItem";
import {Typography} from "@mui/material";

function AddTodolist(props) {
    return (
        <div className='todolist'>
            <Typography variant="h2">
                Ajouter une tâche
            </Typography>
            <AddListItem callback={props.callback} list={props.list}/>
        </div>
    )
}

export default AddTodolist;