import MyList from "./model/MyList";
import {Box, Grid, Typography} from "@mui/material";

function Dashboard(props) {
    let countInProgress = props.list.filter(({status}) => status === false).length;
    let countFinished = props.list.filter(({status}) => status === true).length;

    return (
        <Box className='dashboard'>
            <Typography variant="h2">
                Dashboard
            </Typography>
            <Typography variant="h3">
                Tâches en cours : {countInProgress}
            </Typography>
            <MyList list={props.list} callback={props.callback} finished={false} editable={false}/>
            <Typography variant="h3">
                Tâches terminées : {countFinished}
            </Typography>
            <MyList list={props.list} callback={props.callback} finished={true} editable={false}/>
        </Box>

    )
}

export default Dashboard;