import saveTodolistLocalStorage from "../../helper/TodolistService";
import {useNavigate} from 'react-router-dom';
import {
    Avatar,
    Checkbox,
    IconButton,
    ListItem, ListItemAvatar,
    ListItemButton,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText
} from "@mui/material";
import {Delete, Edit} from "@mui/icons-material";

function MyListItem(props) {
    const navigate = useNavigate();

    let avatar;
    if (props.image) {
        avatar = (<ListItemAvatar>
            <Avatar src={props.image}/>
        </ListItemAvatar>)
    }

    if ((props.finished && props.status) || (!props.finished && !props.status)) {
        return (
            <ListItem key={props.id}>
                <ListItemIcon>
                    <Checkbox
                        edge="start"
                        checked={props.status}
                        tabIndex={-1}
                        onClick={() => checkItem(props)}
                    />
                </ListItemIcon>
                {avatar}
                <ListItemButton role={undefined} dense onClick={() => checkItem(props)}>
                    <ListItemText id={props.title} primary={props.title}/>
                    <ListItemText id={props.description} primary={props.description}/>
                    <ListItemText id={props.deadline} primary={props.deadline}/>
                </ListItemButton>
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="comments" onClick={() => navigate("/edit", {
                        state: {
                            id: props.id,
                            image: props.image,
                            title: props.title,
                            description: props.description,
                            deadline: props.deadline
                        }
                    })}>
                        <Edit/>
                    </IconButton>
                    <IconButton edge="end" aria-label="comments" onClick={() => removeItem(props)}>
                        <Delete/>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>)
    } else {
        return null
    }
}

function checkItem(props) {
    let myList = props.list.slice();
    myList[props.id].status = !myList[props.id].status;
    props.callback(myList);
    saveTodolistLocalStorage(props.list);
}

function removeItem(props) {
    let myList = props.list.slice();
    myList.splice(props.id, 1);
    props.callback(myList);
    saveTodolistLocalStorage(myList);
}

export default MyListItem;
