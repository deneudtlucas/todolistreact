import {useState} from "react";
import saveTodolistLocalStorage from "../../helper/TodolistService";
import {Button, Input, Paper, TextareaAutosize, TextField} from "@mui/material";

function AddListItem(props) {
    const [image, setImage] = useState('');
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [deadline, setDeadline] = useState(new Date(Date.now()).toISOString().split('T')[0]);

    return (
        <div className='add-item'>
            <Paper>
                <TextField
                    type='text'
                    onChange={(e) => setTitle(e.target.value)}
                    value={title}
                    label={"Titre"}
                    required
                />
                <TextField
                    type='text'
                    onChange={(e) => setDescription(e.target.value)}
                    value={description}
                    label={"Description"}
                    required
                />
                <TextField
                    type='date'
                    onChange={(e) => setDeadline(e.target.value)}
                    value={deadline}
                    label={"DeadLine"}
                    required
                />
                <TextField
                    type='url'
                    onChange={(e) => setImage(e.target.value)}
                    value={image}
                    label={"Image"}
                />
                <Button onClick={() => addItem(image, title, description, deadline, props)}>Ajouter une tâche</Button>
            </Paper>
        </div>
    )
}

function addItem(image, title, description, deadline, props) {
    const status = false;
    let item = {image, title, description, deadline, status};
    props.list.push(item);
    props.callback(props.list);
    saveTodolistLocalStorage(props.list);
}

export default AddListItem;