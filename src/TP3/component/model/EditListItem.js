import {useState} from "react";
import saveTodolistLocalStorage from "../../helper/TodolistService";
import {useLocation, useNavigate} from "react-router-dom";
import {Button, Paper, TextField, Typography} from "@mui/material";

function EditListItem(props) {
    const location = useLocation();

    const [image, setImage] = useState(location.state.image);
    const [title, setTitle] = useState(location.state.title);
    const [description, setDescription] = useState(location.state.description);
    const [deadline, setDeadline] = useState(location.state.deadline);

    const navigate = useNavigate();

    let id = location.state.id;

    return (
        <div className='add-item'>
            <Typography variant="h2">
                Editer la tâche
            </Typography>
            <Paper>
                <TextField
                    type='text'
                    onChange={(e) => setTitle(e.target.value)}
                    value={title}
                    label={"Titre"}
                />
                <TextField
                    type='text'
                    onChange={(e) => setDescription(e.target.value)}
                    value={description}
                    label={"Description"}
                />
                <TextField
                    type='date'
                    onChange={(e) => setDeadline(e.target.value)}
                    value={deadline}
                    label={"DeadLine"}
                />
                <TextField
                    type='url'
                    onChange={(e) => setImage(e.target.value)}
                    value={image}
                    label={"Image"}
                />
                <Button onClick={() => {
                    editItem(id, image, title, description, deadline, props, location.state);
                    navigate("/");
                }}>Valider</Button>
            </Paper>
        </div>
    )
}

function editItem(id, image, title, description, deadline, props, currentItem) {
    props.list[id].image = image === null ? currentItem.image : image;
    props.list[id].title = title === null ? currentItem.title : title;
    props.list[id].description = description === null ? currentItem.description : description;
    props.list[id].deadline = deadline === null ? currentItem.deadline : deadline;
    props.callback(props.list);
    saveTodolistLocalStorage(props.list);
}

export default EditListItem;