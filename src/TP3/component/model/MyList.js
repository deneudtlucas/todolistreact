import MyListItem from "./MyListItem";
import {List} from "@mui/material";

function MyList(props) {
    let i = -1;
    return (
        <List>
            {
                props.list.filter(({title}) => props.filter ? title.includes(props.filter) : true)
                    .map(({image, title, description, deadline, status}) => {
                        i++
                        return <MyListItem key={i} image={image} title={title} description={description}
                                           deadline={deadline}
                                           status={status} id={i} list={props.list} callback={props.callback} finished={props.finished} editable={props.editable}/>
                    })
            }
        </List>

    )
}

export default MyList;