import MyList from "./model/MyList";
import SearchBar from "./SearchBar";
import {useState} from "react";
import {Typography} from "@mui/material";

function Todolist(props) {
    const [filter, setFilter] = useState(null);
    return (
        <div className='dashboard'>
            <Typography variant="h2">
                Tâches
            </Typography>
            <SearchBar callback={setFilter}/>
            <MyList list={props.list} callback={props.callback} filter={filter} finished={props.finished}/>
        </div>
    )
}

export default Todolist;