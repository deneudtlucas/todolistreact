import {AppBar, Box, Button, Container, Toolbar} from "@mui/material";
import {useNavigate} from "react-router-dom";

function Navigation() {
    const links = [
        {to: '/', key: 'dashboard', label: 'Dashboard'},
        {to: '/inProgress', key: 'inProgress', label: 'Tâches en cours'},
        {to: '/finished', key: 'finished', label: 'Tâches terminées'},
        {to: '/add', key: 'todolist', label: 'Ajouter une tâche'},
    ]

    const navigate = useNavigate();

    return (
        <AppBar position="static">
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Box sx={{flexGrow: 1, display: {xs: 'none', md: 'flex'}}}>
                        {links.map(({to, key, label}) => (<Button
                            key={key}
                            onClick={() => navigate(to)}
                            sx={{my: 2, color: 'white', display: 'block'}}>
                            {label}
                        </Button>))}
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>)
}

export default Navigation;