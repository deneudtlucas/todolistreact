export default function saveTodolistLocalStorage(todolist) {
    if (todolist) {
        localStorage.setItem('todolist', JSON.stringify(todolist));
    }
}